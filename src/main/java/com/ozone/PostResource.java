package com.ozone;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.ResourceSupport;


class PostResource extends ResourceSupport {
  private final Post post;
  
  public PostResource(Post post) {
    this.post = post;
    String id = post.getId().toString();
    this.add(linkTo(methodOn(PostRestController.class, id).viewOnePost(id)).withSelfRel());
  }
  
  
  public Post getPost() {
    return post;
  }
}