package com.ozone;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

  @Bean
  CommandLineRunner init(PostRepository postRepository) {   
    return (evt) -> {
      Arrays.asList("mark4carter,robert,charlesdawes,joselechuga,matt".split(","))
      .forEach(a ->  {
        String authorUUID = UUID.randomUUID().toString();
        postRepository.save(new Post(a + "'s First Post Ever", authorUUID, new Date(), new Date()));
        postRepository.save(new Post(a + "'s Second Best Post Ever", authorUUID, new Date(), new Date()));        
      });
    };    
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
