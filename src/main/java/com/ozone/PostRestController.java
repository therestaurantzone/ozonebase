package com.ozone;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/posts")
class PostRestController {
  
  private final PostRepository postRepository;
 
  @RequestMapping(value = "/list", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
  Resources<PostResource> readPosts() {
    List<PostResource> postResourceList = postRepository.findAll()
       .stream()
       .map(PostResource::new)
       .collect(Collectors.toList());
    return new Resources<PostResource>(postResourceList);
  }
  
  @RequestMapping(value = "/{postId}/view", method = RequestMethod.GET)
  PostResource viewOnePost(@PathVariable String postId) {
    return new PostResource(this.postRepository.findById(postId));
  }
  
  @CrossOrigin
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  void add(@RequestBody String input) {
    postRepository.save(new Post(input, UUID.randomUUID().toString(), new Date(), new Date()));
  }
  
  @CrossOrigin
  @RequestMapping(value = "/{postId}/delete", method = RequestMethod.DELETE)
  String delete(@PathVariable String postId) {
    postRepository.deleteById(postId);
    return "redirect:/list";
  } 
  
  @CrossOrigin
  @RequestMapping(value = "/{postId}/update", method = RequestMethod.PUT)
  void update(@PathVariable String postId, @RequestBody String input) {
    Post post = postRepository.findById(postId);
    post.setContent(input);
    post.setModified_at(new Date());
    //random for now
    post.setModified_by(UUID.randomUUID().toString());
    postRepository.saveAndFlush(post);
  }

  
  @Autowired
  PostRestController(PostRepository postRepository) {
      this.postRepository = postRepository;
  }
}