package com.ozone;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Post {
  
  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  private String id;
  
  Post () {
    
  }
  
  public Post(String content, String author, Date created_at, Date modified_at) {
    this.content = content;
    this.author = author;
    this.created_at = created_at;
    this.modified_at = modified_at;
    this.deleted_at = null;
  }

  @NotNull
  public String content;
  @NotNull
  public String author;
  public Date created_at;
  public Date modified_at;
  public Date deleted_at;
  public String modified_by;

  public String getId() {
      return id;
  }
  
  public String getAuthor() {
    return author;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
  
  public Date getCreated_at() {
    return created_at;
  }

  public Date getModified_at() {
    return modified_at;
  }
  
  public void setModified_at(Date date) {
    this.modified_at = date;
  }

  public Date getDeleted_at() {
    return deleted_at;
  }

  public String getModified_by() {
    return modified_by;
  }
  
  public void setModified_by(String modified_by) {
    this.modified_by = modified_by;
  }
  
  
 
}
