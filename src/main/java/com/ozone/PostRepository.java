package com.ozone;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

public interface PostRepository extends JpaRepository<Post, Long> {
  Post findById(String id);
  
  @Modifying
  @Transactional
  void deleteById(String id);
}
