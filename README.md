### Generated with Spring Boot (Web, JPA, Rest Repositories, H2, HATEOAS) ###

---------------------

* ``curl localhost:8080/api/posts/list`` -- Query all posts
* ``curl localhost:8080/api/:id/view`` -- Views specific post

Endpoints tested with Postman -- functionality still needed

* ``/api/posts/create`` - Create a new post
* ``/api/posts/:id/delete`` - delete a specific post
* ``/api/posts/:id/update`` - update a specific post